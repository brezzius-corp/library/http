#ifndef __HTTP_H
#define __HTTP_H

#define HTTP_LENGTH_ROOT 32
#define HTTP_LENGTH_METHOD 7
#define HTTP_LENGTH_URI 63
#define HTTP_LENGTH_VERSION 8
#define HTTP_LENGTH_PATH 70
#define HTTP_LENGTH_MIMETYPE 22
#define HTTP_LENGTH_STATUS 31
#define HTTP_LENGTH_GMTTIME 29
#define HTTP_LENGTH_HEADER_NAME 255
#define HTTP_LENGTH_HEADER_VALUE 512
#define HTTP_LENGTH_ATTR_NAME 255
#define HTTP_LENGTH_ATTR_VALUE 512

#define HTTP_SIZE_METHOD 8
#define HTTP_SIZE_METHOD_ALLOW 3
#define HTTP_SIZE_VERSION 1

#define HTTP_DEFAULT_VERSION "HTTP/1.1"
#define HTTP_DEFAULT_PATH "../html"
#define HTTP_DEFAULT_ERRORPATH "/code"

#define SIZE_SESSION 32

#define SIZE_ATTR_NAME 255
#define SIZE_ATTR_VALUE 512

#define HTTP_LENGTH_GET_NAME 255
#define HTTP_LENGTH_GET_VALUE 512

#define HTTP_LENGTH_POST_NAME 255
#define HTTP_LENGTH_POST_VALUE 512

#define HTTP_LENGTH_COOKIE_NAME 255
#define HTTP_LENGTH_COOKIE_VALUE 512

typedef struct stat STAT;

typedef struct HTTP_Attr {
    char name[HTTP_LENGTH_ATTR_NAME];
    char value[HTTP_LENGTH_ATTR_VALUE];
} HTTP_Attr;

typedef struct HTTP_Get {
    char name[HTTP_LENGTH_GET_NAME];
    char value[HTTP_LENGTH_GET_VALUE];
} HTTP_Get;

typedef struct HTTP_Post {
    char name[HTTP_LENGTH_POST_NAME];
    char value[HTTP_LENGTH_POST_VALUE];
} HTTP_Post;

typedef struct HTTP_Cookie {
    char name[HTTP_LENGTH_COOKIE_NAME];
    char value[HTTP_LENGTH_COOKIE_VALUE];
} HTTP_Cookie;

typedef struct {
    char root[HTTP_LENGTH_ROOT + 1];
    char method[HTTP_LENGTH_METHOD + 1];
    char uri[HTTP_LENGTH_URI + 1];
    char path[HTTP_LENGTH_PATH + 1];
    char mimetype[HTTP_LENGTH_MIMETYPE + 1];
    char version[HTTP_LENGTH_VERSION + 1];
    char status[HTTP_LENGTH_STATUS + 1];
    char gmttime[HTTP_LENGTH_GMTTIME + 1];
    char *scookie;
    int len;
    int code;
    int size;
    int cattr;
    int cget;
    int cpost;
    int ccookie;

    HTTP_Attr *attr;
    HTTP_Get *get;
    HTTP_Post *post;
    HTTP_Cookie *cookie;
} HTTP_Header;

HTTP_Header *HTTP_Parse(const char *data);
void HTTP_Init(HTTP_Header *hd, const char *data);
void HTTP_Free(HTTP_Header *hd);
int HTTP_ParseRL(HTTP_Header *hd, const char *data, int *i);
int HTTP_ParseRH(HTTP_Header *hd, const char *data, int *i);
int HTTP_ParseGet(HTTP_Header *hd);
int HTTP_ParsePost(HTTP_Header *hd, const char *data, int *i);
int HTTP_ParseCookie(HTTP_Header *hd);
int HTTP_GetMethod(HTTP_Header *hd);
int HTTP_GetUri(HTTP_Header *hd);
int HTTP_GetVersion(HTTP_Header *hd);
void HTTP_GetRoot(HTTP_Header *hd);
int HTTP_GetMimetype(HTTP_Header *hd);
void HTTP_GetTime(HTTP_Header *hd);
void HTTP_GetCode(HTTP_Header *hd);
char *HTTP_GetHeader(HTTP_Header *hd, const char *name);
char *HTTP_GetGet(HTTP_Header *hd, const char *name);
char *HTTP_GetPost(HTTP_Header *hd, const char *name);
char *HTTP_GetCookie(HTTP_Header *hd, const char *name);
int HTTP_Token(int c, int e);
void HTTP_PrintHeader(HTTP_Header *hd);
char *HTTP_BuildHeader(HTTP_Header *hd);

#endif /* __HTTP_H */

