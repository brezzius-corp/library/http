#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <sys/stat.h>

#include "http.h"
#include "mime.h"
#include "code.h"
#include "token.h"

/* Initialization HTTP_Header structure
 * Parameters : Structure HTTP_Header
 *              Char Request
 * Return value : None
*/
void HTTP_Init(HTTP_Header *hd, const char *data) {
    HTTP_GetTime(hd);

    strcpy(hd->version, HTTP_DEFAULT_VERSION);
    strcpy(hd->path, HTTP_DEFAULT_PATH);

    hd->len = (int) strlen(data);
    hd->code = 200;
    hd->cattr = 0;
    hd->cget = 0;
    hd->cpost = 0;
    hd->ccookie = 0;
}

/* Free memory of HTTP structure
 * Parameters : Strcuture HTTP_Header
 * Return value : None
*/
void HTTP_Free(HTTP_Header *hd) {
    if(hd->cattr != 0)
        free(hd->attr);

    if(hd->cget != 0)
        free(hd->get);

    if(hd->cpost != 0)
        free(hd->post);

    if(hd->ccookie != 0)
        free(hd->cookie);

    free(hd);
}

/* Parse HTTP request
 * Parameter : Request
 * Return value : Structure HTTP_Header
*/
HTTP_Header *HTTP_Parse(const char *data) {
    int i = 0;

    HTTP_Header *hd;

    if(!(hd = malloc(sizeof(HTTP_Header))))
        exit(0);

    HTTP_Init(hd, data);

    if(HTTP_ParseRL(hd, data, &i)) {
        hd->code = 400;
        HTTP_GetCode(hd);
        return hd;
    }

    if(HTTP_ParseRH(hd, data, &i)) {
        hd->code = 400;
        HTTP_GetCode(hd);
        return hd;
    }

    HTTP_GetRoot(hd);

    if(HTTP_GetMethod(hd)) {
        HTTP_GetCode(hd);
        return hd;
    }

    if(HTTP_GetVersion(hd)) {
        HTTP_GetCode(hd);
        return hd;
    }

    if(HTTP_GetUri(hd)) {
        HTTP_GetCode(hd);
        return hd;
    }

    HTTP_ParseGet(hd);

    if(!strcmp(hd->method, "POST"))
        HTTP_ParsePost(hd, data, &i);

    HTTP_ParseCookie(hd);

    return hd;
}

/* Parse HTTP request line
 * Parameters : Structure HTTP_Header
 *              Request
 *              Index
 * Return value : 0 Success
 *                1 Failure
*/
int HTTP_ParseRL(HTTP_Header *hd, const char *data, int *i) {
    int j = 0;

    if(data[0] == ' ')
        return 1;

    /* Method */
    while(data[*i] != ' ') {
        if(*i == hd->len)
            return 1;

        else if(j == HTTP_LENGTH_METHOD) {
            hd->method[0] = '\0';
            break;
        }

        else if(HTTP_Token(data[*i], 0))
            return 1;

        else
            hd->method[j++] = data[(*i)++];
    }

    hd->method[j] = '\0';
    j = 0;

    while(data[*i] != ' ')
        if((*i)++ == hd->len)
            return 1;

    if((*i)++ == hd->len)
        return 1;

    if(data[*i] == ' ')
        return 1;

    /* URI */
    while(data[*i] != ' ') {
        if(*i == hd->len)
            return 1;

        else if(j == HTTP_LENGTH_URI) {
            hd->uri[0] = '\0';
            break;
        }

        else
            hd->uri[j++] = data[(*i)++];
    }

    hd->uri[j] = '\0';
    j = 0;

    while(data[*i] != ' ')
        if((*i)++ == hd->len)
            return 1;

    if((*i)++ >= hd->len - 1)
        return 1;

    if(data[*i] == ' ')
        return 1;

    /* Version */
    while(data[*i] != '\r' || data[*i+1] != '\n') {
        if(*i == hd->len - 1) 
            return 1;

        else if(j == HTTP_LENGTH_VERSION) {
            hd->version[0] = '\0';
            break;
        }

        else if(data[*i] == ' ')
            return 1;

        else
            hd->version[j++] = data[(*i)++];
    }

    hd->version[j] = '\0';

    while(data[*i] != '\r' || data[*i+1] != '\n') {
        if(*i == hd->len - 1 || data[*i] == ' ') {
            (*i)++;
            return 1;
        }

        (*i)++;
    }

    (*i)+=2;

    if(*i >= hd->len - 1)
        return 1;

    return 0;
}

/* Parse HTTP request header
 * Parameters : Structure HTTP_Header
 *              Request
 *              Index
 * Return value : 0 Success
 *                1 Failure
*/
int HTTP_ParseRH(HTTP_Header *hd, const char *data, int *i) {
    char name[HTTP_LENGTH_ATTR_NAME + 1], value[HTTP_LENGTH_ATTR_VALUE + 1];
    int j;

    if(!(hd->attr = malloc(sizeof(HTTP_Attr))))
        exit(0);

    while(data[*i] != '\r' || data[*i+1] != '\n') {
        if(!(hd->attr = realloc(hd->attr, ++hd->cattr * sizeof(HTTP_Attr))))
            exit(0);

        j = 0;

        /* Header field */
        while(data[*i] != ':') {
            if(*i == hd->len)
                return 1;

            if(j == HTTP_LENGTH_ATTR_NAME)
                return 1;

            if(HTTP_Token(data[*i], 0))
                return 1;

            name[j++] = data[(*i)++];
        }

        if(j == 0)
            return 1;

        name[j] = '\0';
        j = 0;

        if((*i)++ == hd->len)
            return 1;

        while(data[*i] == ' ' || data[*i] == '\t')
            if((*i)++ >= hd->len - 1)
                return 1;

        /* Header value */
        while(data[*i] != '\r' || data[*i+1] != '\n') {
            if(*i >= hd->len - 1)
                return 1;

            if(j == HTTP_LENGTH_ATTR_VALUE)
                return 1;

            if(HTTP_Token(data[*i], 1) && data[*i] != ' ' && data[*i] != '\t')
                return 1;

            value[j++] = data[(*i)++];
        }

        value[j] = '\0';

        strcpy(hd->attr[hd->cattr - 1].name, name);
        strcpy(hd->attr[hd->cattr - 1].value, value);

        (*i)+=2;

        if(*i >= hd->len - 1)
            return 1;
    }

    (*i) += 2;

    return 0;
}

/* Get request method
 * Parameter : Structure HTTP_Header
 * Return value : 0 Success
 *                1 Failure
*/
int HTTP_GetMethod(HTTP_Header *hd) {
    int i;
    char method[HTTP_SIZE_METHOD][HTTP_LENGTH_METHOD + 1] = {"GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE"};
    char methodAllow[HTTP_SIZE_METHOD_ALLOW][HTTP_LENGTH_METHOD] = {"GET", "HEAD", "POST"};

    /* Method valididty */
    for(i = 0 ; i < HTTP_SIZE_METHOD ; i++)
        if(!strcmp(hd->method, method[i]))
            break;

    if(i == HTTP_SIZE_METHOD) {
        hd->code = 501;
        return 1;
    }

    /* Method authorization */
    for(i = 0 ; i < HTTP_SIZE_METHOD_ALLOW ; i++)
        if(!strcmp(hd->method, methodAllow[i]))
            break;

    if(i == HTTP_SIZE_METHOD_ALLOW) {
        hd->code = 405;
        return 1;
    }

    return 0;
}

/* Get request URI (+ get params)
 * Parameter : Structure HTTP_Header
 * Return value : 0 Success
 *                1 Failure
*/
int HTTP_GetUri(HTTP_Header *hd) {
    STAT st;
    int i, j, l;

    if(!hd->uri[0]) {
        hd->code = 414;
        return 1;
    }

    if(!strcmp(hd->root, "api"))
        strcat(hd->path, "/api");

    for(i = 0, j = (int) strlen(hd->path), l = (int) strlen(hd->uri) ; i < l && hd->uri[i] != '?' ; i++, j++)
        hd->path[j] = hd->uri[i];

    hd->path[j] = '\0';

    /* Resource available */
    if(strcmp(hd->root, "api")) {
        if(!stat(hd->path, &st)) {
            if(S_ISDIR(st.st_mode)) {
                if(hd->path[strlen(hd->path) - 1] == '/')
                    strcat(hd->path, "index.html");
                else
                    strcat(hd->path, "/index.html");
    
                if(stat(hd->path, &st)) {
                    hd->code = 404;
                    return 1;
                }
            }
        } else {
            hd->code = 404;
            return 1;
        }
    }

    HTTP_GetCode(hd);

    return 0;
}

/* Get request version
 * Parameter : Structure HTTP_Header
 * Return value : 0 Success
 *                1 Failure
*/
int HTTP_GetVersion(HTTP_Header *hd) {
    int i;
    char version[HTTP_SIZE_VERSION][HTTP_LENGTH_VERSION] = {"HTTP/1.1"};

    for(i = 0 ; i < HTTP_SIZE_VERSION ; i++)
        if(!strcmp(hd->version, version[i]))
            break;

    if(i == HTTP_SIZE_VERSION) {
        strcpy(hd->version, HTTP_DEFAULT_VERSION);
        hd->code = 505;
        return 1;
    }

    return 0;
}

/* Get root request
 * Parameter : Structure HTTP_Header
 * Return Value : None
*/
void HTTP_GetRoot(HTTP_Header *hd) {
    const char *host;
    int i = 0, l;

    if((host = HTTP_GetHeader(hd, "Host"))) {
        l = (int) strlen(host);

        while(i < l && i < HTTP_LENGTH_ROOT && host[i] != '.') {
            hd->root[i] = host[i];
            i++;
        }

        if(i > 0)
            hd->root[i] = '\0';
        else
            strcpy(hd->root, "root");

        while(++i < l)
            if(host[i] == '.')
                break;

        if(i == l)
            strcpy(hd->root, "root");
    } else
        strcpy(hd->root, "root");
}

/* Get mimetype of the resource
 * Parameter : Structure HTTP_Header
 * Return value : 0 Success
 *                1 Failure (text/plain as default)
*/
int HTTP_GetMimetype(HTTP_Header *hd) {
    const char *ext;
    int i;

    if((ext = strrchr(hd->path, '.'))) {
        for(i = 0 ; strcmp(mime[i], "EOF") ; i+=2) {
            if(!strcmp(ext, mime[i])) {
                strcpy(hd->mimetype, mime[i+1]);
                return 0;
            }
        }
    }

    strcpy(hd->mimetype, "text/plain");

    return 1;
}

/* Format GMT Time
 * Parameter : Structure HTTP_Header
 * Return value : None
*/
void HTTP_GetTime(HTTP_Header *hd) {
    time_t timestamp = time(NULL);

    strftime(hd->gmttime, sizeof(hd->gmttime), "%a, %m %b %Y %X GMT", gmtime(&timestamp));
}

/* Get code of the HTTP response
 * Parameter : Structure HTTP_Header
 * Return value : None
*/
void HTTP_GetCode(HTTP_Header *hd) {
    STAT st;
    char serr[HTTP_LENGTH_PATH];

    if(hd->code >= 400 && hd->code < 600) {
        sprintf(serr, "%s%s/%d.html", HTTP_DEFAULT_PATH, HTTP_DEFAULT_ERRORPATH, hd->code);
        strcpy(hd->path, serr);
    }

    HTTP_GetMimetype(hd);

    if(!stat(hd->path, &st))
        hd->size = (int) st.st_size;

    strcpy(hd->status, code[(hd->code / 100) - 1][hd->code % 100]);
}

/* Get Get params
 * Parameter : Structure HTTP_Header
 * Return value : 0 Success
 *                1 Failure
*/
int HTTP_ParseGet(HTTP_Header *hd) {
    char name[HTTP_LENGTH_GET_NAME + 1], value[HTTP_LENGTH_GET_VALUE + 1];
    const char *s;
    int i = 0, j, l;

    if((s = strchr(hd->uri, '?'))) {
        s++;

        if(!(hd->get = malloc(sizeof(HTTP_Get))))
            exit(0);

        l = (int) strlen(s);
        while(i < l) {
            if(!(hd->get = realloc(hd->get, ++hd->cget * sizeof(HTTP_Get))))
                exit(0);

            j = 0;

            /* Get field */
            while(s[i] != '=' && i < l) {
                if(i > l - 1)
                    return 1;

                if(j == HTTP_LENGTH_GET_NAME)
                    return 1;

                if(HTTP_Token(s[i], 0))
                    return 1;

                name[j++] = s[i++];
            }

            if(j == 0)
                return 1;

            name[j] = '\0';
            j = 0;

            if(i++ == l)
                return 1;

            /* Get value */
            while(s[i] != '&' && i < l) {
                if(i > l - 1)
                    return 1;

                if(j == HTTP_LENGTH_GET_VALUE)
                    return 1;

                if(HTTP_Token(s[i], 1))
                    return 1;

                value[j++] = s[i++];
            }

            value[j] = '\0';

            strcpy(hd->get[hd->cget - 1].name, name);
            strcpy(hd->get[hd->cget - 1].value, value);

            i++;
        }

        for(i = 0, l = strlen(hd->uri) ; i < l ; i++) {
            if(hd->uri[i] == '?') {
                hd->uri[i] = '\0';
                break;
            }
        }
    }

    return 0;
}

/* Get POST params
 * Parameter : Stucture HTTP_Header
 *             Request
 *             Index
 * Return value : 0 Success
 *                1 Failure
*/
int HTTP_ParsePost(HTTP_Header *hd, const char *data, int *i) {
    char name[HTTP_LENGTH_POST_NAME], value[HTTP_LENGTH_POST_VALUE];
    int j;

    if(!(hd->post = malloc(sizeof(HTTP_Post))))
        exit(0);

    while(*i < hd->len && data[*i] != '\r' && data[*i+1] != '\n') {
        if(!(hd->post = realloc(hd->post, ++hd->cpost * sizeof(HTTP_Post))))
            exit(0);

        j = 0;

        /* Get field */
        while(data[*i] != '=' && *i < hd->len) {
            if(*i > hd->len - 1)
                return 1;

            if(j == HTTP_LENGTH_GET_NAME)
                return 1;

            if(HTTP_Token(data[*i], 0))
                return 1;

            name[j++] = data[(*i)++];
        }

        if(j == 0)
            return 1;

        name[j] = '\0';
        j = 0;

        if((*i)++ == hd->len)
            return 1;

        /* Get value */
        while(data[*i] != '&' && *i < hd->len) {
            if(*i > hd->len - 1)
                return 1;

            if(j == HTTP_LENGTH_GET_VALUE)
                return 1;

            if(HTTP_Token(data[*i], 1))
                return 1;

            value[j++] = data[(*i)++];
        }

        value[j] = '\0';

        strcpy(hd->post[hd->cpost - 1].name, name);
        strcpy(hd->post[hd->cpost - 1].value, value);

        (*i)++;
    }

    return 0;
}

/* Get cookie of request
 * Parameter : Structure HTTP_Header
 * Return value : 0 Success
 *                1 Failure
*/
int HTTP_ParseCookie(HTTP_Header *hd) {
    char name[HTTP_LENGTH_COOKIE_NAME], value[HTTP_LENGTH_COOKIE_VALUE];
    int i = 0, j, l;

    if((hd->scookie = HTTP_GetHeader(hd, "Cookie"))) {
        l = (int) strlen(hd->scookie);

        if(!(hd->cookie = malloc(sizeof(HTTP_Cookie))))
            return 1;

        while(i < l) {
            if(!(hd->cookie = realloc(hd->cookie, ++hd->ccookie * sizeof(HTTP_Cookie))))
                return 1;

            /* Delete empty spaces */
            while((i < l) && hd->scookie[i] == ' ')
                i++;

            j = 0;

            while((i < l) && (hd->scookie[i] != '=') && (j < HTTP_LENGTH_COOKIE_NAME))
                name[j++] = hd->scookie[i++];
    
            name[j] = '\0';
    
            i++;
            j = 0;
    
            while((i < l) && (hd->scookie[i] != ';') && (j < HTTP_LENGTH_COOKIE_VALUE))
                value[j++] = hd->scookie[i++];
    
            i++;
            value[j] = '\0';
    
            strcpy(hd->cookie[hd->ccookie - 1].name, name);
            strcpy(hd->cookie[hd->ccookie - 1].value, value);
        }
    }

    return 0;
}

/* Get header params
 * Parameter : Structure HTTP_Header
 *             Value to search
 * Return value : Value found on Success
 *                Null on Failure
*/
char *HTTP_GetHeader(HTTP_Header *hd, const char *name) {
    int i;

    for(i = 0 ; i < hd->cattr ; i++)
        if(!strcmp(hd->attr[i].name, name))
            return hd->attr[i].value;

    return NULL;
}

/* Set header params
 * Parameter : Structure HTTP_Header
 *             Name to set
 *             Value to set
 * Return value : None
*/
void HTTP_SetHeader(HTTP_Header *hd, const char *name, const char *value) {
    if(!(hd->attr = realloc(hd->attr, ++hd->cattr * sizeof(HTTP_Attr))))
        exit(0);

    strcpy(hd->attr[hd->cattr - 1].name, name);
    strcpy(hd->attr[hd->cattr - 1].value, value);
}

/* Get GET params
 * Parameter : Structure HTTP_Header
 *             Value to search
 * Return value : Value found on Success
 *                Null on Failure
*/
char *HTTP_GetGet(HTTP_Header *hd, const char *name) {
    int i;

    for(i = 0 ; i < hd->cget ; i++)
        if(!strcmp(hd->get[i].name, name))
            return hd->get[i].value;

    return NULL;
}

/* Get POST params
 * Parameter : Structure HTTP_Header
 *             Value to search
 * Return value : Value found on Success
 *                Null on Failure
*/
char *HTTP_GetPost(HTTP_Header *hd, const char *name) {
    int i;

    for(i = 0 ; i < hd->cpost ; i++)
        if(!strcmp(hd->post[i].name, name))
            return hd->post[i].value;

    return NULL;
}

/* Get cookie params
 * Parameter : Structure HTTP_Header
 *             Value to search
 * Return value : Value found on Success
 *                Null on Failure
*/
char *HTTP_GetCookie(HTTP_Header *hd, const char *name) {
    int i;

    if(hd->ccookie == 0)
        return NULL;

    for(i = 0 ; i < hd->ccookie ; i++)
        if(!strcmp(hd->cookie[i].name, name))
            return hd->cookie[i].value;

    return NULL;
}

/* Verify if character is an available token
 * Parameter : Character
 *             Extension to UTF8
 * Return value : 0 Success
 *                1 Failure
*/
int HTTP_Token(int c, int e) {
    if(e && c > 0x20 && c < 0x100 && c != 0x7F)
        return 0;

    else if(c > 0x00 && c < 0x7F && token[c])
        return 0;

    return 1;
}


/* Print parse header
 * Parameter : Structure HTTP_Header
 * Return value : None
*/
void HTTP_PrintHeader(HTTP_Header *hd) {
    int i;

    printf("Root : %s\n", hd->root);
    printf("Method : %s\nURI : %s\nVersion : %s\n", hd->method, hd->uri, hd->version);
    printf("Path : %s\n", hd->path);
    printf("Mimetype : %s\n", hd->mimetype);
    printf("Code : %d\nStatus : %s\n", hd->code, hd->status);
    printf("GMT Time : %s\n", hd->gmttime);

    printf("\nHeader :\n");
    for(i = 0 ; i < hd->cattr ; i++)
        printf("  %s => %s\n", hd->attr[i].name, hd->attr[i].value);

    printf("\nGET Params : \n");
    for(i = 0 ; i < hd->cget ; i++)
        printf("  %s => %s\n", hd->get[i].name, hd->get[i].value);

    printf("\nPOST Params :\n");
    for(i = 0 ; i < hd->cpost ; i++)
        printf("  %s => %s\n", hd->post[i].name, hd->post[i].value);

    printf("\nCOOKIE Params :\n");
    for(i = 0 ; i < hd->ccookie ; i++) 
        printf("  %s => %s\n", hd->cookie[i].name, hd->cookie[i].value);
}

/* Build HTTP header
 * Parameter : Structure HTTP_Header
 * Return value : Response header on Success
 *                Null on Failure
*/
char *HTTP_BuildHeader(HTTP_Header *hd) {
    char *data;
    char buffer[1024];
    int i;

    /* Allocation de data */
    if(!(data = malloc(4096 * sizeof(char))))
        exit(EXIT_FAILURE);

    sprintf(data, "%s %d %s\r\nDate: %s\r\nServer: Brezzius\r\nContent-Type: %s\r\nConnection: close\r\nContent-Length: %d\r\n", hd->version, hd->code, hd->status, hd->gmttime, hd->mimetype, hd->size);

    /* Construction de l'en-tête en fonction du mimetype */
    if(!strcmp(hd->mimetype, "text/html") || !strcmp(hd->mimetype, "text/plain") || !strcmp(hd->mimetype, "text/css") || !strcmp(hd->mimetype, "application/javascript") || !strcmp(hd->mimetype, "application/json") || !strcmp(hd->mimetype, "application/vnd.apple.mpegurl") || !strcmp(hd->mimetype, "video/mp2t"))
        sprintf(buffer, "Cache-Control: no-cache\r\nAccess-Control-Allow-Origin: *\r\n");
    else if(!strcmp(hd->mimetype, "image/png") || !strcmp(hd->mimetype, "image/jpg") || !strcmp(hd->mimetype, "font/woff2"))
        sprintf(buffer, "Accept-Ranges: bytes\r\nCache-Control: max-age=900\r\n");
    else if(!strcmp(hd->mimetype, "video/mp4"))
        sprintf(buffer, "Accept-Ranges: bytes\r\nCache-Control: max-age=1800\r\n");

    strcat(data, buffer);

    /* Set Cookie */
    for(i = 0 ; i < hd->ccookie ; i++) {
        sprintf(buffer, "Set-Cookie: %s=%s\n", hd->cookie[i].name, hd->cookie[i].value);
        strcat(data, buffer);
    }

    strcat(data, "\r\n");

    return data;
}

