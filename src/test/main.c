#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../main/http.h"
#include "test.h"

#define ITERATION_MAX 100

int main(void) {
    HTTP_Header *hd;
    char *header;
    int i, j;

    printf("\033[H\033[2JRunning for %d iteration...\n", ITERATION_MAX * NB_TEST);
    sleep(2);

    for(i = 0 ; i < ITERATION_MAX ; i++) {
        for(j = 0 ; j < NB_TEST ; j++) {
            printf("=================================== STEP %.d  ===================================\n", j+1);
            printf("\nCommand : [%s]\n", test[j]);
    
            if((hd = HTTP_Parse(test[j]))) {
                HTTP_PrintHeader(hd);
                printf("%s\n", HTTP_BuildHeader(hd));
                printf("\n");
    
                header = HTTP_BuildHeader(hd);
                HTTP_Free(hd);
                free(header);
            }
        }
    }

    return 0;
}

