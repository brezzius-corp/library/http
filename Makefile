# Makefile

include config.mk

CC=gcc
CFLAGS=-W -Wall -Wextra -Werror -pedantic -ansi -fPIC
CFLAGS_TEST=-W -Wall -Wextra -Werror -pedantic
LDFLAGS=-shared
LDFLAGS_TEST=bin/libhttp.so
LIBRARY=libhttp.so
LIBRARY_TEST=libhttp_test
INCLUDE=http.h
SRC_MAIN=src/main
SRC_TEST=src/test
SRC_BIN=bin

all: package test

package: libhttp
	$(CC) -o $(SRC_BIN)/$(LIBRARY) $(SRC_BIN)/http.o $(LDFLAGS)

test: package libhttp_test
	$(CC) -o $(SRC_BIN)/$(LIBRARY_TEST) $(SRC_BIN)/main.o $(LDFLAGS_TEST)

libhttp: $(SRC_MAIN)/http.c $(SRC_MAIN)/$(INCLUDE) $(SRC_MAIN)/mime.h $(SRC_MAIN)/code.h $(SRC_MAIN)/token.h
	mkdir -p bin
	$(CC) -o $(SRC_BIN)/http.o -c $(SRC_MAIN)/http.c $(CFLAGS)

libhttp_test: $(SRC_TEST)/main.c $(SRC_TEST)/test.h
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_TEST)/main.c $(CFLAGS_TEST)

install: all
	mkdir -p $(DESTDIR)$(PREFIX)/lib
	cp -f $(SRC_BIN)/$(LIBRARY) $(DESTIR)$(PREFIX)/lib
	cp -f $(SRC_MAIN)/$(INCLUDE) $(DESTIR)$(PREFIX)/include
	chown root:root $(DESTIR)$(PREFIX)/lib/$(LIBRARY)
	chmod 755 $(DESTIR)$(PREFIX)/lib/$(LIBRARY)
	chown root:root $(DESTIR)$(PREFIX)/include/$(INCLUDE)
	chmod 644 $(DESTIR)$(PREFIX)/include/$(INCLUDE)

uninstall:
	rm -f $(DESTIR)$(PREFIX)/lib/$(LIBRARY)
	rm -f $(DESTIR)$(PREFIX)/include/$(INCLUDE)

clean:
	rm -f $(SRC_BIN)/*.o
	rm -f $(SRC_BIN)/$(LIBRARY)
	rm -f $(SRC_BIN)/$(LIBRARY_TEST)

