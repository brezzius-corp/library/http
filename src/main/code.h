#ifndef __CODE_H
#define __CODE_H

#define HTTP_LENGTH_CODE 32

#define HTTP_CODE_100 "Continue"
#define HTTP_CODE_101 "Switching Protocols"

#define HTTP_CODE_200 "OK"
#define HTTP_CODE_201 "Created"
#define HTTP_CODE_202 "Accepted"
#define HTTP_CODE_203 "Non-Authoritative Information"
#define HTTP_CODE_204 "No Content"
#define HTTP_CODE_205 "Reset Content"
#define HTTP_CODE_206 "Partial Content"

#define HTTP_CODE_300 "Multiple Choices"
#define HTTP_CODE_301 "Moved Permanently"
#define HTTP_CODE_302 "Found"
#define HTTP_CODE_303 "See Other"
#define HTTP_CODE_304 "Not Modified"
#define HTTP_CODE_305 "Use Proxy"
#define HTTP_CODE_307 "Temporary Redirect"
#define HTTP_CODE_308 "Permanent Redirect"
#define HTTP_CODE_310 "Too many Redirects"

#define HTTP_CODE_400 "Bad Request"
#define HTTP_CODE_401 "Unauthorized"
#define HTTP_CODE_402 "Payment Required"
#define HTTP_CODE_403 "Forbidden"
#define HTTP_CODE_404 "Not Found"
#define HTTP_CODE_405 "Method Not Allowed"
#define HTTP_CODE_406 "Not Acceptable"
#define HTTP_CODE_407 "Proxy Authentication Required"
#define HTTP_CODE_408 "Request Time-out"
#define HTTP_CODE_409 "Conflict"
#define HTTP_CODE_410 "Gone"
#define HTTP_CODE_411 "Length Required"
#define HTTP_CODE_412 "Precondition Failed"
#define HTTP_CODE_413 "Request Entity Too Large"
#define HTTP_CODE_414 "Request-URI Too Long"
#define HTTP_CODE_415 "Unsupported Media Type"
#define HTTP_CODE_416 "Requested range unsatisfiable"
#define HTTP_CODE_417 "Expectation failed"
#define HTTP_CODE_426 "Upgrade Required"
#define HTTP_CODE_428 "Precondition Required"
#define HTTP_CODE_429 "Too Many Requests"
#define HTTP_CODE_431 "Request Header Fields Too Large"
#define HTTP_CODE_451 "Unavailable For Legal Reasons"

#define HTTP_CODE_500 "Internal Server Error"
#define HTTP_CODE_501 "Not Implemented"
#define HTTP_CODE_502 "Bad Gateway ou Proxy Error"
#define HTTP_CODE_503 "Service Unavailable"
#define HTTP_CODE_504 "Gateway Time-out"
#define HTTP_CODE_505 "HTTP Version not supported"
#define HTTP_CODE_506 "Variant Also Negotiates"
#define HTTP_CODE_509 "Bandwidth Limit Exceeded"
#define HTTP_CODE_510 "Not extended"
#define HTTP_CODE_511 "Network authentication required"

char code[][24][HTTP_LENGTH_CODE] = {
    {HTTP_CODE_100, HTTP_CODE_101},

    {HTTP_CODE_200, HTTP_CODE_201, HTTP_CODE_202, HTTP_CODE_203, HTTP_CODE_204, HTTP_CODE_205, HTTP_CODE_206},

    {HTTP_CODE_300, HTTP_CODE_301, HTTP_CODE_302, HTTP_CODE_303, HTTP_CODE_304,
     HTTP_CODE_305, HTTP_CODE_307, HTTP_CODE_308, HTTP_CODE_310},

    {HTTP_CODE_400, HTTP_CODE_401, HTTP_CODE_402, HTTP_CODE_403, HTTP_CODE_404, HTTP_CODE_405,
     HTTP_CODE_406, HTTP_CODE_407, HTTP_CODE_408, HTTP_CODE_409, HTTP_CODE_410, HTTP_CODE_411,
     HTTP_CODE_412, HTTP_CODE_413, HTTP_CODE_414, HTTP_CODE_415, HTTP_CODE_416, HTTP_CODE_417,
     HTTP_CODE_426, HTTP_CODE_428, HTTP_CODE_429, HTTP_CODE_431, HTTP_CODE_451},

    {HTTP_CODE_500, HTTP_CODE_501, HTTP_CODE_502, HTTP_CODE_503, HTTP_CODE_504,
     HTTP_CODE_505, HTTP_CODE_506, HTTP_CODE_509, HTTP_CODE_510, HTTP_CODE_511}
};

#endif /* __CODE_H */

