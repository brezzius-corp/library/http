# HTTP Library

Library for parse HTTP requests

## Requirements

In order to build http library you need C89 library and header files.

## Installation

Edit config.mk to match your local setup (libhttp in installed into the /usr/local namespace by default).

Enter the following commands to download and build libhttp :

```
git clone https://gitlab.com/brezzius-corp/library/http
cd http
sudo make install
```

## Linking

Enter the following command to using and linking libhttp :

`gcc -lhttp [...]`

## Uninstall

Enter the following command to uninstall http library :

`sudo make uninstall`
