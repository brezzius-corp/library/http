#ifndef __MIME_H
#define __MIME_H

const char mime[][255] = {
    ".html", "text/html", ".css", "text/css", ".js", "application/javascript", ".xml", "application/xml", ".png", "image/png", ".jpg", "image/jpg", ".woff2", "font/woff2",
    ".mp4", "video/mp4", ".m3u8", "text/plain", ".ts", "text/plain",
    "EOF"
};

#endif /* __MIME_H */

